# Minecraft server in Docker container with Docker Compose

## Table of contents <!-- omit in toc -->

- [Minecraft server in Docker container with Docker Compose](#minecraft-server-in-docker-container-with-docker-compose)
  - [Prerequisites](#prerequisites)
  - [Informations](#informations)
  - [Start server](#start-server)
  - [Login into server console](#login-into-server-console)
  - [Change server configuration](#change-server-configuration)
    - [Change server type](#change-server-type)
    - [Change server RAM](#change-server-ram)
    - [Change server version](#change-server-version)

## Prerequisites

First, you should have Docker and Docker-compose already installed on your server.

---

## Informations

For this project is use the [minecraft-server](https://hub.docker.com/r/itzg/minecraft-server) docker image from [itzg](https://hub.docker.com/u/itzg) (Where you can find many images for minecraft server).

I used to his [full documentation](https://github.com/itzg/docker-minecraft-server/blob/master/README.md) on this [Github account](https://github.com/itzg).

I want to use Traefik reverse proxy to easilly map my server on specific adress, but nothing work, if you find any fix for this contact me :) (Please).

---

## Start server

Basicly, run server config with

    docker-compose up --build

And when you configuration is ok (as you want), you can simply use

    docker-compose up -d

(I just hope you know docker commands)

---

## Login into server console

To access to the server command,

    docker exec -it my-minecraft-server bash

will not work, you should use

    docker attach my-minecraft-server

To leave server console just press `CTRL+Q` or `CTRL+P` or if you use Portainer you can directly attach container by pressing **Attach** button.

---

## Change server configuration

### Change server type

FOR MORE INFORMATION JUST CHECK [FULL DOCUMENTATION](https://github.com/itzg/docker-minecraft-server/blob/master/README.md)

For simply change type of you server, you can edit this value

    TYPE: "BUKKIT"

All servers type are listed on his documentation.

    Forge,
    Spigot,
    Airplane,
    Etc..

### Change server RAM

For `BUKKIT` is prefer is minimum of 4G of RAM

### Change server version

For change version you can simply edit this line :

    VERSION: "1.16.4"

All versions are listed on this [FULL DOCUMENTATION](https://github.com/itzg/docker-minecraft-server/blob/master/README.md), but you should refer to [this table](https://github.com/itzg/docker-minecraft-server/blob/master/README.md#running-minecraft-server-on-different-java-version) to manage java version of your server easily.
